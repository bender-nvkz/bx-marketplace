<?php
namespace BxMarketplace;

/**
 * Класс объявлен для будущих реализаций,
 * в качестве основной контролирующей структуры решений.
 *
 * Class Manager
 *
 * @package BxMarketplace
 * @author Artem Iksanov <iksanov@rekuz.ru>
 * @version 0.0.1
 */
class Manager
{

}
