<?php

namespace BxMarketplace\Fixtures;

use BxMarketplace\AdminMenuProvider;
use CBXVirtualIo;


/**
 * Абстрактный класс реализации поддержки решения Композер-модуля
 * в виде Битрикс-модуля в системе.
 *
 * Class AbstractSolution
 *
 * @package BxMarketplace\Fixtures
 * @author Artem Iksanov <iksanov@rekuz.ru>
 * @version 1.1.5
 */
abstract class AbstractSolution
{
	
	/** @var array */
	protected static $listen;
	
	/** @var array */
	protected static $bindings;
	
	/** @var string */
	private static $solutionClassFile;
	
	/** @var string */
	private static $solutionPath;
	
	/** @var string */
	private static $adminDirName = 'admin';
	
	/**
	 * @param \Bitrix\Main\EventManager|null $eventManager
	 */
	final public static function register(\Bitrix\Main\EventManager $eventManager = null)
	{
		
		/** @global \Composer\Autoload\ClassLoader $COMPOSER */
		
		global $COMPOSER;
		self::$solutionClassFile = $COMPOSER->findFile(static::class);
		self::$solutionPath      = with(CBXVirtualIo::GetInstance())
			->GetFile(with(CBXVirtualIo::GetInstance())->ExtractPathFromPath(self::$solutionClassFile))
			->GetPath();
		
		if (!$eventManager) {
			$eventManager = \Bitrix\Main\EventManager::getInstance();
		}
		
		self::registerAdmin($eventManager);
		self::boot($eventManager);
		self::bind($eventManager);
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function boot(\Bitrix\Main\EventManager $eventManager)
	{
		
		if (is_array(static::$listen)) {
			foreach (static::$listen as $event => $handler) {
				$arEvent   = explode('.', $event);
				$arHandler = explode('::', $handler);
				$eventManager->addEventHandler(current($arEvent), next($arEvent),
					[current($arHandler), next($arHandler)]);
			}
		}
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function bind(\Bitrix\Main\EventManager $eventManager)
	{
		
		/** @var \BxMarketplace\Fixtures\AbstractSolution $bindingSolutionClass */
		
		if (is_array(static::$bindings)) {
			foreach (static::$bindings as $bindingSolutionClass) {
				$bindingSolutionClass::register($eventManager);
			}
		}
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function registerAdmin(\Bitrix\Main\EventManager $eventManager)
	{
		
		/** @var \CBXVirtualDirectoryFileSystem $adminDir */
		
		$adminDirPath    = with(CBXVirtualIo::GetInstance())->CombinePath(self::$solutionPath, self::$adminDirName);
		$menuHandlerFunc = 'initAdmin';
		
		$adminDir = with(CBXVirtualIo::GetInstance())->GetDirectory($adminDirPath);
		
		if ($adminDir->IsExists() && !$adminDir->IsEmpty()) {
			static::$listen['main.OnBuildGlobalMenu'] = static::class . '::' . $menuHandlerFunc;
		}
	}
	
	/**
	 * @param array $aGlobalMenu
	 * @param array $aModuleMenu
	 */
	final public static function initAdmin(&$aGlobalMenu, &$aModuleMenu)
	{
		
		/** @global \CUser $USER */
		
		global $USER;
		
		/** @todo Add rights checking */
		if ($USER->IsAdmin()) {
			with(new AdminMenuProvider(static::class))->initSolutionMenu($aGlobalMenu, $aModuleMenu);
		}
	}
	
	/**
	 * @return string|null
	 */
	public static function getSolutionPath()
	{
		
		return self::$solutionPath;
	}
	
	/**
	 * @return string|null
	 */
	public static function getAdminDirName()
	{
		
		return self::$adminDirName;
	}
	
}
