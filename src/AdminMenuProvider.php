<?php

namespace BxMarketplace;

use Bitrix\Main\Context;
use CBXVirtualIo;
use Bitrix\Main\Web\Uri;


/**
 * Класс реализует интеграцию страниц административной части решения
 * в главное меню администратора "на лету".
 *
 * Class AdminMenuProvider
 *
 * @package BxMarketplace
 * @author Artem Iksanov <iksanov@rekuz.ru>
 * @version 1.3.0
 */
final class AdminMenuProvider
{
	
	/** @var \BxMarketplace\Fixtures\AbstractSolution */
	private $solutionClass;
	
	/** @var \CBXVirtualDirectoryFileSystem */
	private $absoluteAdminDir;
	
	/** @var string */
	private $relativeAdminPath;
	
	/** @var array */
	private $aGlobalMenu;
	
	/** @var array */
	private $aModuleMenu;
	
	/** @var string */
	private $menuInfoFile = '.menu.php';
	
	/** @var array */
	private $aMenuInfo;
	
	/** @var array */
	private $solutionAdminFiles;
	
	/**
	 * AdminMenuProvider constructor.
	 *
	 * @param string $solutionClass
	 */
	public function __construct($solutionClass)
	{
		
		/** @var \BxMarketplace\Fixtures\AbstractSolution $solutionClass */
		
		$this->solutionClass = $solutionClass;
		
		$this->absoluteAdminDir = with(CBXVirtualIo::GetInstance())->GetDirectory(with(CBXVirtualIo::GetInstance())->CombinePath($solutionClass::getSolutionPath(),
			$solutionClass::getAdminDirName()));
		
		$this->relativeAdminPath = substr_replace($this->absoluteAdminDir->GetPath(), '', 0,
				strlen(\Bitrix\Main\Application::getDocumentRoot())) . '/admin';
	}
	
	/**
	 * @param array $aGlobalMenu
	 * @param array $aModuleMenu
	 */
	public function initSolutionMenu(&$aGlobalMenu, &$aModuleMenu)
	{
		
		/** @var array $aAdminMenuLinks */
		
		$this->aGlobalMenu = &$aGlobalMenu;
		$this->aModuleMenu = &$aModuleMenu;
		
		include with(CBXVirtualIo::GetInstance())->CombinePath($this->absoluteAdminDir->GetPathWithName(),
			$this->menuInfoFile);
		
		if (!is_set($aAdminMenuLinks)) {
			return;
		}
		
		$this->aMenuInfo = &$aAdminMenuLinks;
		
		if (Context::getCurrent()->getRequest()->getRequestedPageDirectory() == $this->relativeAdminPath ||
			Context::getCurrent()->getRequest()->getRequestedPage() == '/bitrix/admin/get_menu.php') {
			$this->makeAbsoluteMenu($this->aGlobalMenu);
			$this->makeAbsoluteMenu($this->aModuleMenu);
		}
		
		foreach ($this->absoluteAdminDir->GetChildren() as $child) {
			if (!$child->IsDirectory()) {
				if ($child->GetExtension() == 'php' && $child->GetName() !== $this->menuInfoFile) {
					$this->solutionAdminFiles[] = $child;
					$fileName                   = pathinfo($child->GetName(), PATHINFO_FILENAME);
					if (isset($this->aMenuInfo[ $fileName ])) {
						$this->includeMenuFile($child);
					}
				}
			}
		}
	}
	
	/**
	 * @param array $aMenu
	 */
	private function makeAbsoluteMenu(&$aMenu)
	{
		
		foreach ($aMenu as &$menuItem) {
			if (isset($menuItem['url']) && !starts_with($menuItem['url'], '/')) {
				$menuItem['url'] = BX_ROOT . '/admin/' . $menuItem['url'];
			}
			if (isset($menuItem['items'])) {
				$this->makeAbsoluteMenu($menuItem['items']);
			}
		}
	}
	
	/**
	 * @param \CBXVirtualFileFileSystem $adminFile
	 */
	private function includeMenuFile(\CBXVirtualFileFileSystem $adminFile)
	{
		
		/** @var \Bitrix\Main\Web\Uri $url */
		
		$fileName = pathinfo($adminFile->GetName(), PATHINFO_FILENAME);
		
		$url = with(new Uri(with(CBXVirtualIo::GetInstance())->CombinePath($this->relativeAdminPath,
			$adminFile->GetName())))->addParams(['lang' => LANGUAGE_ID]);
		
		$menuItem        = $this->aMenuInfo[ $fileName ];
		$menuItem['url'] = $url->getPathQuery();
		
		if (Context::getCurrent()->getRequest()->getRequestedPage() == $url->getPath()) {
			$menuItem['_active'] = true;
			$this->setActiveItem($menuItem);
		}
		
		/** @todo Check 'more_url' property */
		
		$this->aModuleMenu[] = $menuItem;
		
	}
	
	/**
	 * @param $menuItem
	 */
	private function setActiveItem($menuItem)
	{
		
		/** @global \CAdminMenu $adminMenu */
		
		global $adminMenu;
		
		$globalActive = $this->aGlobalMenu[ $menuItem['parent_menu'] ];
		
		$adminMenu->aActiveSections = [
			$globalActive['items_id'] => [
				'menu_id'      => isset($globalActive['menu_id']) ? $globalActive['menu_id'] : null,
				'items_id'     => isset($globalActive['items_id']) ? $globalActive['items_id'] : null,
				'page_icon'    => isset($globalActive['page_icon']) ? $globalActive['page_icon'] : null,
				'text'         => isset($globalActive['text']) ? $globalActive['text'] : null,
				'url'          => isset($globalActive['url']) ? $globalActive['url'] : null,
				'skip_chain'   => isset($globalActive['skip_chain']) ? $globalActive['skip_chain'] : null,
				'help_section' => isset($globalActive['help_section']) ? $globalActive['help_section'] : null,
			],
			'_active'                 => [
				'menu_id'      => isset($menuItem['menu_id']) ? $menuItem['menu_id'] : null,
				'page_icon'    => isset($menuItem['page_icon']) ? $menuItem['page_icon'] : null,
				'text'         => isset($menuItem['text']) ? $menuItem['text'] : null,
				'url'          => isset($menuItem['url']) ? $menuItem['url'] : null,
				'skip_chain'   => isset($menuItem['skip_chain']) ? $menuItem['skip_chain'] : null,
				'help_section' => isset($menuItem['help_section']) ? $menuItem['help_section'] : null,
			],
		];
	}
	
}
